console.log("Activity in JS Arrays");

let student = [];
console.log(student);

/* Solution 3:

Create an addStudent() function that will accept a name of the student and add it to the student array.

*/

function addStudent(name){
	student.push(name)
}

addStudent("John");
addStudent("Jane");
addStudent("Joe");

console.log(student);


/* Solution #4:

Create a countStudents() function that will print the total number of students in the array.

*/

function countStudents(){
	// console.log(student.length)

	return `There are ${student.length} students currently enrolled`
}

console.log(countStudents())



/* Solution #5

Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.

*/

function printStudents(){
	student.sort()
	// console.log(student)


	student.forEach(function(name){
		console.log(name)
	})
}

printStudents()



/*Solution #6:

Create a findStudent() function that will do the following:
	a. Search for a student name when a keyword is given (filter method).

	b. If one match is found print the message studentName is an enrollee.

	c. If multiple matches are found print the message studentNames are enrollees.
	
	d. If no match is found print the message studentName is not an enrollee.
	
	e. The keyword given should not be case sensitive.

*/

function findStudent(keyword){
	//e.
	let word = keyword.toLowerCase();

	// a.
	let match = student.filter(function(name){
		// console.log(name)
		// console.log(name.includes(keyword))
		let lcName = name.toLowerCase();

		return lcName.includes(word)
	})
	console.log(match);


	//b.
	if(match.length === 1){
		console.log(`${match} is an enrollee`);

	//c.
	}else if(match.length > 1){
		console.log(`${match} are enrollees`);

	//d.
	} else {
		console.log(`${keyword} is not an enrollee.`);
	}
}

findStudent("ane");



/* STRETCH GOALS

Solution #1:

Create an addSection() function that will add a section to all students in the array with the format of studentName - Section A (map method).

*/

function addSection(section){

	let sectionedStudents = student.map(function(name){
		// console.log(name)

		return `${name} - Section ${section}`
	})

	console.log(sectionedStudents)
}

addSection("A");



/* STRETCH GOALS

Solution #2:

Create a removeStudent() Function that will do the following:
	a. Capitalize the first letter of the user’s input (toUpperCase and slice methods).
	
	b. Retrieve the index of the student to be removed (indexOf method).
	
	c. Remove the student from the array (splice method).

	d. Print a message that the studentName was removed from the student’s list.

*/

function removeStudent(name){
	// console.log(name)

	//a.
	let firstLetter = name.slice(0, 1).toUpperCase();
	let remainingLetter = name.slice(1)

	let capName = firstLetter + remainingLetter


	//b.
	let studentIndex = student.indexOf(capName);
	console.log(studentIndex);

	//c.
	if(studentIndex !== -1){
		student.splice(studentIndex, 1);

		//d.
		console.log(`${capName} was removed from the student’s list`)
	}
}

removeStudent("John");

console.log(student);